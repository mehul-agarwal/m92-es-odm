'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _autoBind = _interopRequireDefault(require("auto-bind"));

var _ResponseBody = _interopRequireDefault(require("./ResponseBody"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

class ESController {
  constructor(Model) {
    this.Model = Model;
    (0, _autoBind.default)(this);
  }

  createIndex(request, response, next) {
    var _this = this;

    return _asyncToGenerator(function* () {
      var {
        Model
      } = _this;
      var esResponse = yield Model.createIndex();
      response.body = new _ResponseBody.default(200, 'Create Index Success', esResponse);
      next();
    })();
  }

  removeIndex(request, response, next) {
    var _this2 = this;

    return _asyncToGenerator(function* () {
      var {
        Model
      } = _this2;
      var esResponse = yield Model.removeIndex();
      response.body = new _ResponseBody.default(200, 'Remove Index Success', esResponse);
      next();
    })();
  }

  create(request, response, next) {
    var _this3 = this;

    return _asyncToGenerator(function* () {
      var {
        Model
      } = _this3;
      var {
        body: {
          data
        }
      } = request;
      var esResponse = yield Model.create(data);
      response.body = new _ResponseBody.default(201, 'Create Success', esResponse);
      next();
    })();
  }

  bulkCreate(request, response, next) {
    var _this4 = this;

    return _asyncToGenerator(function* () {
      var {
        Model
      } = _this4;
      var {
        body: {
          data
        }
      } = request;
      var esResponse = yield Model.bulkCreate(data);
      response.body = new _ResponseBody.default(200, 'Bulk Create Success', esResponse);
      next();
    })();
  }

  recreate(request, response, next) {
    var _this5 = this;

    return _asyncToGenerator(function* () {
      var {
        Model
      } = _this5;
      var {
        body: {
          data
        }
      } = request;
      var esResponse = yield Model.recreate(data);
      response.body = new _ResponseBody.default(200, 'Recreate Success', esResponse);
      next();
    })();
  }

  findById(request, response, next) {
    var _this6 = this;

    return _asyncToGenerator(function* () {
      var {
        Model
      } = _this6;
      var {
        params
      } = request;
      var {
        id
      } = params;
      var esResponse = yield Model.findById(id);
      response.body = new _ResponseBody.default(200, 'Find by Id Success', esResponse);
      next();
    })();
  }

  search(request, response, next) {
    var _this7 = this;

    return _asyncToGenerator(function* () {
      var {
        Model
      } = _this7;
      var {
        query
      } = request;
      var searchQuery = {
        match: query
      };
      var searchBody = {
        query: searchQuery
      };
      var esResponse = yield Model.search(searchBody);
      response.body = new _ResponseBody.default(200, 'Search Success', esResponse);
      next();
    })();
  }

  list(request, response, next) {
    var _this8 = this;

    return _asyncToGenerator(function* () {
      var {
        Model
      } = _this8;
      var esResponse = yield Model.list();
      response.body = new _ResponseBody.default(200, 'List Success', esResponse);
      next();
    })();
  }

  update(request, response, next) {
    var _this9 = this;

    return _asyncToGenerator(function* () {
      var {
        Model
      } = _this9;
      var {
        body: {
          data
        }
      } = request;
      var {
        params
      } = request;
      var {
        id
      } = params; // TODO: Validate data.id and params.id

      data.id = id;
      var esResponse = yield Model.update(data);
      response.body = new _ResponseBody.default(200, 'Update Success', esResponse);
      next();
    })();
  }

  bulkUpdate(request, response, next) {
    var _this10 = this;

    return _asyncToGenerator(function* () {
      var {
        Model
      } = _this10;
      var {
        body: {
          data
        }
      } = request;
      var esResponse = yield Model.bulkUpdate(data);
      response.body = new _ResponseBody.default(200, 'Bulk Update Success', esResponse);
      next();
    })();
  }

  remove(request, response, next) {
    var _this11 = this;

    return _asyncToGenerator(function* () {
      var {
        Model
      } = _this11;
      var {
        params
      } = request;
      var {
        id
      } = params;
      yield Model.remove(id);
      response.body = new _ResponseBody.default(200, 'Remove Success');
      next();
    })();
  }

  bulkRemove(request, response, next) {
    var _this12 = this;

    return _asyncToGenerator(function* () {
      var {
        Model
      } = _this12;
      var {
        body: {
          data
        }
      } = request;
      var esResponse = yield Model.bulkRemove(data);
      response.body = new _ResponseBody.default(200, 'Bulk Remove Success', esResponse);
      next();
    })();
  }

}

exports.default = ESController;