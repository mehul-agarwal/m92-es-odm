'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ESClassWrapper;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function ESClassWrapper(Class, wrapperProps) {
  var {
    INDEX,
    TYPE,
    SETTINGS,
    PROPERTIES
  } = wrapperProps;

  var indexFunc = () => INDEX;

  var typeFunc = () => TYPE;

  var settingsFunc = () => _objectSpread({}, SETTINGS);

  var propsFunc = () => _objectSpread({}, PROPERTIES);

  Class.__defineGetter__('_index', indexFunc);

  Class.__defineGetter__('_type', typeFunc);

  Class.__defineGetter__('_settings', settingsFunc);

  Class.__defineGetter__('_properties', propsFunc);
}