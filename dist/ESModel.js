'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _ESClient = _interopRequireDefault(require("./ESClient"));

var _ESError = _interopRequireDefault(require("./ESError"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var DEFAULT_SETTING = {
  index: {
    number_of_shards: 5,
    number_of_replicas: 1
  }
};

class ESModel {
  constructor(CONFIG, Class) {
    var {
      _index = '',
      _type = '',
      _settings = {},
      _properties = {}
    } = Class;
    this.CONFIG = CONFIG;
    this.Class = Class;
    this.index = _index;
    this.type = _type;
    this.settings = _settings;
    this.properties = _properties; // Method Hard-binding

    this.createIndex = this.createIndex.bind(this);
    this.removeIndex = this.removeIndex.bind(this);
    this.create = this.create.bind(this);
    this.createWithIndex = this.createWithIndex.bind(this);
    this.bulkCreate = this.bulkCreate.bind(this);
    this.recreate = this.recreate.bind(this);
    this.findById = this.findById.bind(this);
    this.list = this.list.bind(this);
    this.search = this.search.bind(this);
    this.update = this.update.bind(this);
    this.bulkUpdate = this.bulkUpdate.bind(this);
    this.remove = this.remove.bind(this);
    this.bulkRemove = this.bulkRemove.bind(this);
    this.removeBy = this.removeBy.bind(this);
  }

  createIndex(_index) {
    var _this = this;

    return _asyncToGenerator(function* () {
      var {
        CONFIG,
        index,
        settings,
        properties
      } = _this;
      var Client = new _ESClient.default(CONFIG);
      var body = {
        settings: Object.assign({}, DEFAULT_SETTING, settings),
        mappings: {
          properties
        }
      };
      var params = {
        index: _index || index,
        body
      };

      try {
        var esResponse = yield Client.indices.create(params);
        Client.close();
        return esResponse;
      } catch (error) {
        Client.close();
        throw new _ESError.default(error);
      }
    })();
  }

  removeIndex(_index) {
    var _this2 = this;

    return _asyncToGenerator(function* () {
      var {
        CONFIG,
        index,
        settings,
        properties
      } = _this2;
      var Client = new _ESClient.default(CONFIG);
      var body = {
        settings: Object.assign({}, DEFAULT_SETTING, settings),
        mappings: {
          properties: properties
        }
      };
      var params = {
        index: _index || index,
        body
      };

      try {
        var esResponse = yield Client.indices.delete(params);
        Client.close();
        return esResponse;
      } catch (error) {
        Client.close();
        throw new _ESError.default(error);
      }
    })();
  }

  create(attrs, dynamicIndex) {
    var _this3 = this;

    return _asyncToGenerator(function* () {
      var {
        CONFIG,
        Class,
        index,
        type
      } = _this3;
      var Client = new _ESClient.default(CONFIG);
      var body = new Class(attrs);
      var {
        id
      } = body;
      var params = {
        refresh: true,
        index: dynamicIndex || index,
        type,
        id,
        body
      };

      try {
        yield Client.create(params);
        Client.close();
        return body;
      } catch (error) {
        Client.close();
        throw new _ESError.default(error);
      }
    })();
  }

  recreate(attrs) {
    var _this4 = this;

    return _asyncToGenerator(function* () {
      try {
        var indexExist = yield _this4._indexExist();

        if (indexExist) {
          yield _this4.removeIndex();
        }

        yield _this4.createIndex();
        var createResponse = yield _this4.bulkCreate(attrs);
        return createResponse;
      } catch (error) {
        throw new _ESError.default(error);
      }
    })();
  }

  createWithIndex(attrs, options) {
    var _this5 = this;

    return _asyncToGenerator(function* () {
      var {
        Class
      } = _this5;
      var body = new Class(attrs);

      try {
        var index = _this5._getDynamicIndex(body, options);

        yield _this5.createIndex(index);
        var createResponse = yield _this5.create(attrs, index);
        return createResponse;
      } catch (error) {
        throw new _ESError.default();
      }
    })();
  }

  bulkCreate(attrs) {
    var _this6 = this;

    return _asyncToGenerator(function* () {
      var {
        CONFIG,
        Class,
        index: _index,
        type: _type
      } = _this6;
      var body = [];

      _lodash.default.each(attrs, doc => {
        var {
          id: _id
        } = doc;
        var actionObj = {
          create: {
            _index,
            _id,
            _type
          }
        };
        body.push(actionObj);
        var dataObj = new Class(doc);
        body.push(dataObj);
      });

      var params = {
        refresh: true,
        body
      };
      var Client = new _ESClient.default(CONFIG);

      try {
        var bulkCreateResponse = yield Client.bulk(params);
        var {
          items
        } = bulkCreateResponse;
        var responseBody = {
          errorPresent: false,
          errors: [],
          items: []
        };

        _lodash.default.each(items, (responseRecord, index) => {
          var operation = Object.keys(responseRecord)[0];
          var item = responseRecord[operation];
          var bodyItem = body[(index + 1) * 2 - 1];

          if (item.status === 201) {
            responseBody.items.push(new Class(_objectSpread(_objectSpread({}, bodyItem), {}, {
              id: item._id
            })));
          } else {
            responseBody.errorPresent = true;
            responseBody.errors.push({
              item: new Class(bodyItem),
              message: item.error.reason
            });
          }
        });

        Client.close();
        return responseBody;
      } catch (error) {
        Client.close();
        throw new _ESError.default(error);
      }
    })();
  }

  findById(id) {
    var _this7 = this;

    return _asyncToGenerator(function* () {
      var {
        CONFIG,
        Class,
        index,
        type
      } = _this7;
      var Client = new _ESClient.default(CONFIG);
      var params = {
        refresh: true,
        index,
        type,
        id
      };

      try {
        var {
          _source
        } = yield Client.get(params);
        Client.close();
        var instance = new Class(_source);
        return instance;
      } catch (error) {
        Client.close();
        throw new _ESError.default(error);
      }
    })();
  }

  list() {
    var _this8 = this;

    return _asyncToGenerator(function* () {
      var {
        CONFIG,
        Class,
        index,
        type
      } = _this8;
      var Client = new _ESClient.default(CONFIG);
      var body = {
        query: {
          match_all: {}
        }
      };
      var params = {
        index,
        type,
        body
      };

      try {
        var esRecord = yield Client.search(params);
        var {
          hits: {
            hits
          }
        } = esRecord;
        hits = _lodash.default.chain(hits).sortBy('_score').map(hit => {
          var {
            _score,
            _source
          } = hit;
          var instance = new Class(_source);
          instance._score = _score;
          return instance;
        }).value();
        Client.close();
        return hits;
      } catch (error) {
        Client.close();
        throw new _ESError.default(error);
      }
    })();
  }

  search(body) {
    var _this9 = this;

    return _asyncToGenerator(function* () {
      var {
        CONFIG,
        Class,
        index,
        type
      } = _this9;
      var Client = new _ESClient.default(CONFIG);
      var params = {
        index,
        type,
        body
      };

      try {
        var esRecord = yield Client.search(params);
        var {
          hits: {
            hits
          }
        } = esRecord;
        hits = _lodash.default.chain(hits).sortBy('_score').map(hit => {
          var {
            _score,
            _source
          } = hit;
          var instance = new Class(_source);
          instance._score = _score;
          return instance;
        }).value();
        Client.close();
        return hits;
      } catch (error) {
        Client.close();
        throw new _ESError.default(error);
      }
    })();
  }

  update(attrs) {
    var _this10 = this;

    return _asyncToGenerator(function* () {
      var {
        CONFIG,
        index,
        type,
        Class
      } = _this10;
      var Client = new _ESClient.default(CONFIG);
      var doc = new Class(attrs);
      var {
        id
      } = doc;
      var body = {
        doc
      };
      var params = {
        refresh: true,
        index,
        type,
        id,
        body
      };

      try {
        yield Client.update(params);
        Client.close();
        return doc;
      } catch (error) {
        Client.close();
        throw new _ESError.default(error);
      }
    })();
  }

  bulkUpdate(attrs) {
    var _this11 = this;

    return _asyncToGenerator(function* () {
      var {
        CONFIG,
        Class,
        index: _index,
        type: _type
      } = _this11;
      var body = [];

      _lodash.default.each(attrs, doc => {
        var {
          id: _id
        } = doc;
        var actionObj = {
          update: {
            _index,
            _id,
            _type
          }
        };
        body.push(actionObj);
        var dataObj = {
          doc: new Class(doc)
        };
        body.push(dataObj);
      });

      var params = {
        refresh: true,
        body
      };
      var Client = new _ESClient.default(CONFIG);

      try {
        var bulkCreateResponse = yield Client.bulk(params);
        var {
          items
        } = bulkCreateResponse;
        var responseBody = {
          errorPresent: false,
          errors: [],
          items: []
        };

        _lodash.default.each(items, (responseRecord, index) => {
          var operation = Object.keys(responseRecord)[0];
          var item = responseRecord[operation];
          var bodyItem = body[(index + 1) * 2 - 1].doc;

          if (item.status === 200) {
            responseBody.items.push(new Class(_objectSpread(_objectSpread({}, bodyItem), {}, {
              id: item._id
            })));
          } else {
            responseBody.errorPresent = true;
            responseBody.errors.push({
              item: new Class(bodyItem),
              message: item.error.reason
            });
          }
        });

        Client.close();
        return responseBody;
      } catch (error) {
        Client.close();
        throw new _ESError.default(error);
      }
    })();
  }

  remove(id) {
    var _this12 = this;

    return _asyncToGenerator(function* () {
      var {
        CONFIG,
        index,
        type
      } = _this12;
      var Client = new _ESClient.default(CONFIG);
      var params = {
        refresh: true,
        index,
        type,
        id
      };

      try {
        yield Client.delete(params);
        Client.close();
        return {
          status: 'Success'
        };
      } catch (error) {
        Client.close();
        throw new _ESError.default(error);
      }
    })();
  }

  bulkRemove(ids) {
    var _this13 = this;

    return _asyncToGenerator(function* () {
      var {
        CONFIG,
        index: _index,
        type: _type
      } = _this13;
      var body = [];

      _lodash.default.each(ids, (_ref) => {
        var {
          id: _id
        } = _ref;
        var actionObj = {
          delete: {
            _index,
            _type,
            _id
          }
        };
        body.push(actionObj);
      });

      var params = {
        refresh: true,
        body
      };
      var Client = new _ESClient.default(CONFIG);

      try {
        var bulkDeleteResponse = yield Client.bulk(params);
        var {
          items
        } = bulkDeleteResponse;
        var responseBody = {
          errorPresent: false,
          errors: [],
          items: []
        };

        _lodash.default.each(items, (responseRecord, index) => {
          var operation = Object.keys(responseRecord)[0];
          var item = responseRecord[operation];

          if (item.status === 200) {
            responseBody.items.push(item._id);
          } else {
            responseBody.errorPresent = true;
            responseBody.errors.push({
              id: item._id,
              message: item.result
            });
          }
        });

        Client.close();
        return responseBody;
      } catch (error) {
        Client.close();
        throw new _ESError.default(error);
      }
    })();
  }

  removeBy(query) {
    var _this14 = this;

    return _asyncToGenerator(function* () {
      var {
        CONFIG,
        index
      } = _this14;
      var Client = new _ESClient.default(CONFIG);
      var body = {
        query
      };
      var params = {
        refresh: true,
        index,
        body
      };

      try {
        var esResponse = yield Client.deleteByQuery(params);
        Client.close();
        return esResponse;
      } catch (error) {
        Client.close();
        throw new _ESError.default(error);
      }
    })();
  }

  _indexExist(_index) {
    var _this15 = this;

    return _asyncToGenerator(function* () {
      var {
        CONFIG,
        index,
        settings,
        properties
      } = _this15;
      var Client = new _ESClient.default(CONFIG);
      var body = {
        settings: Object.assign({}, DEFAULT_SETTING, settings),
        mappings: {
          properties: properties
        }
      };
      var params = {
        index: _index || index,
        body
      };

      try {
        var esResponse = yield Client.indices.exists(params);
        Client.close();
        return esResponse;
      } catch (error) {
        Client.close();
        throw new _ESError.default(error);
      }
    })();
  }

  _getDynamicIndex(body, options) {
    var {
      getDynamicIndex
    } = options;

    if (typeof getDynamicIndex !== 'function') {
      var err = new _ESError.default(new Error("Require 'getDynamicIndex()' to Create Document"));
      throw err;
    }

    var index = getDynamicIndex(body);

    if (!index) {
      var _err = new _ESError.default(500, "Invalid Index Found: '" + index + "'");

      throw _err;
    } else {
      return index;
    }
  }

}

exports.default = ESModel;