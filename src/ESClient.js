'use strict'

import { Client } from 'elasticsearch'

export default class ESClient {
  constructor (CONFIG) {
    const {
      ACCESS_KEY_ID = '',
      SECRET_ACCESS_KEY = '',
      REGION = '',
      HOST = ''
    } = CONFIG

    const ES_CONFIG = {
      service: 'es',
      region: REGION,
      host: HOST,
      accessKeyId: ACCESS_KEY_ID,
      secretAccessKey: SECRET_ACCESS_KEY,
      ssl: { pfx: [] }
    }

    return new Client(ES_CONFIG)
  }
}
